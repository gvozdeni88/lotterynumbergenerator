﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace LotteryNumberGenerator
{
    public class Program
    {
        static void Main(string[] args)
        {
            var numberOfBalls = GetUserInput();

            var sortedNumbers = GenerateAndSortNumbers(numberOfBalls);

            Console.WriteLine("The following numbers have been generated:");
            foreach (var number in sortedNumbers)
            {
                Console.BackgroundColor = ColourPicker.GetColour(number);
                Console.Write(number + " ");
                Console.ResetColor();
            }

            Console.ReadLine();
        }

        private static List<int> GenerateAndSortNumbers(int numberOfBalls)
        {
            HashSet<int> numbers = new HashSet<int>();
            Random randomizer = new Random();

            while (numbers.Count < numberOfBalls)
            {
                numbers.Add(randomizer.Next(1, 49));
            }

            List<int> sortedNumbers = numbers.ToList();
            sortedNumbers.Sort();
            return sortedNumbers;
        }

        private static int GetUserInput()
        {
            Console.WriteLine(
                "The default number of balls is 6. If you want to change the number please type it in and press enter key. \nOtherwise just press enter key");
            var input = Console.ReadLine();

            int numberOfBalls = 6;

            if (!int.TryParse(input, out numberOfBalls))
            {
                Console.WriteLine("User input was not valid"); //TODO: manage errors if users enters the string and prompt again
                numberOfBalls = 6;
            }
            return numberOfBalls;
        }
    }
}
