﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LotteryNumberGenerator
{
    public static class ColourPicker
    {
        public static ConsoleColor GetColour(int number)
        {

            // todo : switch statement
            if (number > 9 && number <= 19)
                return Console.BackgroundColor = ConsoleColor.Blue;
            if (number >= 20 && number <= 29)
                return Console.BackgroundColor = ConsoleColor.DarkCyan;
            if (number >= 30 && number <= 39)
                return Console.BackgroundColor = ConsoleColor.Green;
            if (number >= 40 && number <= 49)
                return Console.BackgroundColor = ConsoleColor.Yellow;

            return Console.BackgroundColor = ConsoleColor.Gray;
        }
    }
}
